import React from "react"
import 'bootstrap/dist/css/bootstrap.min.css';
import Navigation from "../src/Components/Navigation/Navigation"
import { BrowserRouter, Route, Switch } from "react-router-dom";
import DowntimeSearch from "../src/Components/Searcher/DowntimeSearch"
import Emailer from "../src/Components/EmailForm/Emailer"
import EmailStorage from "./Components/EmailStorage/EmailStorage"
import Home from "./Pages/Home/Home";

const App = () => {


  return (
    <BrowserRouter>
      <div className="mainContainer">
        <Navigation />
        <div className="pageContent">
          <Switch>
          <Route path="/" exact component={Home} />
            <Route path="/home" exact component={Home} />
            <Route path="/downtimesearch" exact component={DowntimeSearch} />
            <Route path="/emailer" exact component={Emailer} />
            <Route path="/emailStorage" exact component={EmailStorage} />
          </Switch>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
