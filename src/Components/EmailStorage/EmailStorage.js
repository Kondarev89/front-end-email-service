import React from "react"
import { Table, Form, Button } from 'react-bootstrap'
import "./EmailStorage.css"


const EmailStorage = () => {
  return (
    <div >
      <Form.Control as="select" id="HorD" style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}>
        <option style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}> Send </option>
        <option style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}> Scheduled </option>
        <option style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}> Drafts</option>
      </Form.Control >
      <div id="label1">
        <Form.Label id="sr" style={{ color: "black", fontSize: 22, fontWeight: 'bold', fontFamily: "Tahoma" }}>Search for E-Mails :</Form.Label>
        <Form.Control placeholder="name@example.com" id="sb" style={{ color: "white", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }} />

        <Button variant="primary" id="btn" style={{ fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}> Search </Button>

      </div>

      <Table striped bordered hover >
        <thead>
          <tr>
            <th style={{ color: "black", fontSize: 18, fontWeight: "bold",fontFamily: "Tahoma" }}>#</th>
            <th style={{ color: "black", fontSize: 18, fontWeight: "bold",fontFamily: "Tahoma" }}>E-mail</th>
            <th style={{ color: "black", fontSize: 18, fontWeight: "bold",fontFamily: "Tahoma" }}> E-mail Tittle </th>
            <th style={{ color: "black", fontSize: 18, fontWeight: "bold",fontFamily: "Tahoma" }}> Problem</th>
            <td style={{ color: "black", fontSize: 18, fontWeight: "bold", fontFamily: "Tahoma" }}>Send or Scheduled</td>
            <th style={{ color: "black", fontSize: 18, fontWeight: "bold",fontFamily: "Tahoma" }}>Date</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">iankostankov1@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Reply : Issues with our company site Hosting</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Hosting</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Scheduled</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>
          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">iwanpeshkow2@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Reply : How can the hosting is so expensive?</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }} > Hosting</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Scheduled</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>
          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">liudmilajonkovich3@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>  Reply : Your databases are the worst thing that happened to me in my life...</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Database</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Send</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>

          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">stehakovashkq4@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Reply : You call this hosting?</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Scheduled</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>
          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">evelinamirchoha5@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>  Reply : Database dead ? FIX IT ASAP</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Database</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Send</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>
          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">teodortwombisheff6@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Reply : Jesus people do i need to call ya every day?</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Cloud </td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Scheduled</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>

          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">velizarpotrew7@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>  Reply : Please answer me... im desperate</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Scheduled</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>
          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">harrypotwmalek8@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Reply : I will find you and kill you!</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Cloud</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Send</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>
          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">igorkarroshw9@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Reply : Question about my hosting plan.</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Send</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>

          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">penkawrozkova10@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>  Reply : 100 bucks for this crappy hosting? you gota be joking...</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Scheduled</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>
          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">pennypaytonk11@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Reply : Im really angry...contact me or suffer</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Cloud</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Send</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>
          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">trishstrapunk12@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Reply : Potato servers? Probably...!</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Scheduled</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>

          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">lillyrezons13@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Reply : GIVE ME MY MONEY BACK!!!</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Send</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>
          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">teanyoreeves14@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Reply : Im starting to get anoyed to call you every day...!</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Database</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Send</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>
          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">rambosrednogorsky15@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Reply : Take my money now!</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Scheduled</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>
          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">weronikalibashik16@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Reply : Question about our Database.</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Database</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Scheduled</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>
          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">isabelaporterw17@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Reply : are you kidding me with this support?</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Cloud </td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Send</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>
          <tr>
            <input type="checkbox" className="a" />
            <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/mailbox">jinnyWooshbumz18@gmail.com</a></td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Reply : Database dead...every single day!</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Database</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Send</td>
            <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
          </tr>
        </tbody>
      </Table>
    </div>
  )
}

export default EmailStorage;
