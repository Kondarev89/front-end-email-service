import React, { useState } from "react"
import Form from "react-bootstrap/Form"
import { Button, Table } from "react-bootstrap"
import "./Form.css"


const DowntimeSearch = () => {


    return (
        <div >
            <Form.Control as="select" id="HorD"  style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}>
                <option style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>WEB HOSTING</option>
                <option style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>DATABASE</option>
                <option style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>CLOUD</option>
                
            </Form.Control >
            <div id="label1">
                <Form.Label id="sr" style={{ color: "black", fontSize: 22, fontWeight: 'bold', fontFamily: "Tahoma" }}>Search for Clients :</Form.Label>
                <Form.Control placeholder="name@example.com" id="sb" style={{ color: "white", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }} />
                <Button variant="primary" id="btn" style={{ color: "white", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}> Search </Button>
            </div>
            
            <Table striped bordered hover >
                
                <thead>
                    <tr>
                        <th style={{ color: "black", fontWeight: 'bold', fontSize: 18, fontFamily: "Tahoma" }}>#</th>
                        <th style={{ color: "black", fontWeight: 'bold', fontSize: 18, fontFamily: "Tahoma" }}>E-mail</th>
                        <th style={{ color: "black", fontWeight: 'bold', fontSize: 18, fontFamily: "Tahoma" }}>Position</th>
                        <th style={{ color: "black", fontWeight: 'bold', fontSize: 18, fontFamily: "Tahoma" }}>Provided Serives </th>
                        <th style={{ color: "black", fontWeight: 'bold', fontSize: 18, fontFamily: "Tahoma" }}>Date</th>
                    </tr>
                </thead>
                <tbody style={{ backgroundColor: "white" }}>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">iankostankov1@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Really Bad Tech Support</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Cloud</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">iwanpeshkow2@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Bad Tech Support</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">liudmilajonkovich3@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Tech support</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Database</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">stehakovashkq4@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Tech support</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">evelinamirchoha5@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Hosting</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Database</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">teodortwombisheff6@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Coding Enthusiast</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>

                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">velizarpotrew7@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>QA enthusiast</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">harrypotwmalek8@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Wizzard</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Cloud</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">igorkarroshw9@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Noob</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>

                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">penkawrozkova10@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Gladiator</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">pennypaytonk11@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Nice lady</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Database</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">trishstrapunk12@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Angry person</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>

                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">lillyrezons13@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>CTO</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">teanyoreeves14@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Nobody</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Database</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">rambosrednogorsky15@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Front End Wizz</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">weronikalibashik16@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Hardcoding genious</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>Database</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">isabelaporterw17@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }} > Tech Enthusiast</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                    <tr>
                        <input type="checkbox" className="a" />
                        <td style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}><a href="/emailer">jinnyWooshbumz18@gmail.com</a></td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Teck Lead</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}> Hosting</td>
                        <td style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}>3.18.2021</td>
                    </tr>
                </tbody>
            </Table>
            <Form.Group controlId="selectBox2">
            </Form.Group>

        </div>


    )
};

export default DowntimeSearch;
