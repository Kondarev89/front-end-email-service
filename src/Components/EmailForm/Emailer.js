import React from "react"
import { Form, Button } from "react-bootstrap"
import "./Emailer.css"


class Emailer extends React.Component {
    state = {
        items: [],
        value: "",
        error: null
    };

    handleKeyDown = evt => {
        if (["Enter", "Tab", ","].includes(evt.key)) {
            evt.preventDefault();

            var value = this.state.value.trim();

            if (value && this.isValid(value)) {
                this.setState({
                    items: [...this.state.items, this.state.value],
                    value: ""
                });
            }
        }
    };

    handleChange = evt => {
        this.setState({
            value: evt.target.value,
            error: null
        });
    };

    handleDelete = item => {
        this.setState({
            items: this.state.items.filter(i => i !== item)
        });
    };

    handlePaste = evt => {
        evt.preventDefault();

        var paste = evt.clipboardData.getData("text");
        var emails = paste.match(/[\w\d\.-]+@[\w\d\.-]+\.[\w\d\.-]+/g);

        if (emails) {
            var toBeAdded = emails.filter(email => !this.isInList(email));

            this.setState({
                items: [...this.state.items, ...toBeAdded]
            });
        }
    };

    isValid(email) {
        let error = null;

        if (this.isInList(email)) {
            error = `${email} has already been added.`;
        }

        if (!this.isEmail(email)) {
            error = `${email} is not a valid email address.`;
        }

        if (error) {
            this.setState({ error });

            return false;
        }

        return true;
    }

    isInList(email) {
        return this.state.items.includes(email);
    }

    isEmail(email) {
        return /[\w\d\.-]+@[\w\d\.-]+\.[\w\d\.-]+/.test(email);
    }

    render() {
        return (
            <div id="bg" >
                <div id="emailer" className="container rounded border-10 ">
                    <p style={{ color: "black", fontSize: 45, fontWeight: 'bold', fontFamily: "Tahoma", textAlign: "center" }}>E-Mail Form</p>
                    <div className="col-lg-12 text-left">

                        <Form.Group >
                            <Form.Label style={{ color: "black", fontSize: 24, fontWeight: 'bold', fontFamily: "Tahoma", textAlign: "center" }}> To E-mail address :</Form.Label>
                            <>
                                {this.state.items.map(item => (
                                    <div className="tag-item" key={item}>
                                        {item}
                                        <button
                                            type="button"
                                            className="button"
                                            onClick={() => this.handleDelete(item)}
                                        >
                                            &times;
              </button>
                                    </div>
                                ))}
                                <Form.Control style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}
                                    className={"input " + (this.state.error && " has-error")}
                                    value={this.state.value}
                                    placeholder="Type or paste email addresses and press `Enter`..."
                                    onKeyDown={this.handleKeyDown}
                                    onChange={this.handleChange}
                                    onPaste={this.handlePaste}
                                />

                                {this.state.error && <p className="error">{this.state.error}</p>}
                            </>
                            <br></br>
                            <Form.Label style={{ color: "black", fontSize: 24, fontWeight: 'bold', fontFamily: "Tahoma", textAlign: "center" }}> E-Mail Tittle :</Form.Label>
                            <Form.Control type="email" placeholder="Sending you this email about..." style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }} />
                            <Form.Label style={{ color: "black", fontSize: 24, fontWeight: 'bold', fontFamily: "Tahoma", textAlign: "center" }}>New Message :</Form.Label>

                            <Form.Control as="textarea" placeholder="Write your message here!" id="ta" style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }} />
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-upload" viewBox="0 0 20 20" id="o">
                                    <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z" />
                                    <path d="M7.646 1.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 2.707V11.5a.5.5 0 0 1-1 0V2.707L5.354 4.854a.5.5 0 1 1-.708-.708l3-3z" />
                                </svg>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-emoji-smile" viewBox="0 0 20 20" id="o">
                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                    <path d="M4.285 9.567a.5.5 0 0 1 .683.183A3.498 3.498 0 0 0 8 11.5a3.498 3.498 0 0 0 3.032-1.75.5.5 0 1 1 .866.5A4.498 4.498 0 0 1 8 12.5a4.498 4.498 0 0 1-3.898-2.25.5.5 0 0 1 .183-.683zM7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z" />
                                </svg>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-fonts" viewBox="0 0 20 20" id="o">
                                    <path d="M12.258 3H3.747l-.082 2.46h.478c.26-1.544.76-1.783 2.694-1.845l.424-.013v7.827c0 .663-.144.82-1.3.923v.52h4.082v-.52c-1.162-.103-1.306-.26-1.306-.923V3.602l.43.013c1.935.062 2.434.301 2.694 1.846h.479L12.258 3z" />
                                </svg>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-image" viewBox="0 0 20 20" id="o">
                                    <path d="M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                    <path d="M2.002 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2h-12zm12 1a1 1 0 0 1 1 1v6.5l-3.777-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12V3a1 1 0 0 1 1-1h12z" />
                                </svg>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-bucket-fill" viewBox="0 0 20 20" id="o">
                                    <path d="M2.522 5H2a.5.5 0 0 0-.494.574l1.372 9.149A1.5 1.5 0 0 0 4.36 16h7.278a1.5 1.5 0 0 0 1.483-1.277l1.373-9.149A.5.5 0 0 0 14 5h-.522A5.5 5.5 0 0 0 2.522 5zm1.005 0a4.5 4.5 0 0 1 8.945 0H3.527z" />
                                </svg>
                            </div>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma", textAlign: "center" }}>Send or Schedule E-Mail :</Form.Label>
                            <Form.Control as="select" placeholder="Send E-Mail" style={{ color: "black", fontSize: 18,fontWeight: "bold", fontFamily: "Tahoma", textAlign: "center" }} id="sm">
                                <option style={{ color: "black", fontSize: 18, fontFamily: "Tahoma", textAlign: "center" }}>Send E-Mail</option>
                                <option style={{ color: "black", fontSize: 18, fontFamily: "Tahoma", textAlign: "center" }}>Schedule E-Mail</option>
                                <option style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }}> Draft E-Mail</option>
                            </Form.Control>
                        </Form.Group>
                        <div id="eb">
                            <Button variant="primary" style={{ fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }} > Send </Button>
                        </div>
                    </div>
                </div>

            </div >

        );
    }
}


export default Emailer;
// Client: Tanyo Vladimirov Kondarev  Company: "BulgariaMWare" number: 0883416915  Position: Hardcoding Specialist email: tynkp79@abv.bg<






// const Emailer = () => {

//     return (
//         <div id="bg">
//             <div id="emailer" className="container rounded border-10 ">
//                 <p style={{ color: "black", fontSize: 45, fontWeight: 'bold', fontFamily: "Tahoma", textAlign: "center" }}>Email Form</p>
//                 <div className="col-lg-12 text-left">

//                     <Form.Group >
//                         <Form.Label style={{ color: "black", fontSize: 24, fontWeight: 'bold', fontFamily: "Tahoma", textAlign: "center" }}> To E-mail address :</Form.Label>
//                         <Form.Control type="email" placeholder="name@example.com" style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}/>
//                         <Form.Label style={{ color: "black", fontSize: 24, fontWeight: 'bold', fontFamily: "Tahoma", textAlign: "center" }}> E-Mail Tittle :</Form.Label>
//                         <Form.Control type="email" placeholder="Sending you this email about..." style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}/>
//                         <Form.Label style={{ color: "black", fontSize: 24, fontWeight: 'bold', fontFamily: "Tahoma" , textAlign: "center" }}>New Message :</Form.Label>

//                         <Form.Control as="textarea" placeholder="Write your message here!" id="ta" style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" }}/>
//                         <div>
//                             <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-upload" viewBox="0 0 20 20" id="o">
//                                 <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z" />
//                                 <path d="M7.646 1.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 2.707V11.5a.5.5 0 0 1-1 0V2.707L5.354 4.854a.5.5 0 1 1-.708-.708l3-3z" />
//                             </svg>
//                             <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-emoji-smile" viewBox="0 0 20 20" id="o">
//                                 <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
//                                 <path d="M4.285 9.567a.5.5 0 0 1 .683.183A3.498 3.498 0 0 0 8 11.5a3.498 3.498 0 0 0 3.032-1.75.5.5 0 1 1 .866.5A4.498 4.498 0 0 1 8 12.5a4.498 4.498 0 0 1-3.898-2.25.5.5 0 0 1 .183-.683zM7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z" />
//                             </svg>
//                             <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-fonts" viewBox="0 0 20 20" id="o">
//                                 <path d="M12.258 3H3.747l-.082 2.46h.478c.26-1.544.76-1.783 2.694-1.845l.424-.013v7.827c0 .663-.144.82-1.3.923v.52h4.082v-.52c-1.162-.103-1.306-.26-1.306-.923V3.602l.43.013c1.935.062 2.434.301 2.694 1.846h.479L12.258 3z" />
//                             </svg>
//                             <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-image" viewBox="0 0 20 20" id="o">
//                                 <path d="M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
//                                 <path d="M2.002 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2h-12zm12 1a1 1 0 0 1 1 1v6.5l-3.777-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12V3a1 1 0 0 1 1-1h12z" />
//                             </svg>
//                             <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-bucket-fill" viewBox="0 0 20 20" id="o">
//                                 <path d="M2.522 5H2a.5.5 0 0 0-.494.574l1.372 9.149A1.5 1.5 0 0 0 4.36 16h7.278a1.5 1.5 0 0 0 1.483-1.277l1.373-9.149A.5.5 0 0 0 14 5h-.522A5.5 5.5 0 0 0 2.522 5zm1.005 0a4.5 4.5 0 0 1 8.945 0H3.527z" />
//                             </svg>
//                         </div>
//                     </Form.Group>
//                     <Form.Group>
//                         <Form.Label style={{ color: "black", fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" , textAlign: "center" }}>Send or Schedule E-Mail :</Form.Label>
//                         <Form.Control as="select" placeholder="Send E-Mail" style={{ color: "black", fontSize: 24, fontFamily: "Tahoma" , textAlign: "center" }} id="sm">
//                             <option style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" , textAlign: "center" }}>Send E-Mail</option>
//                             <option style={{ color: "black", fontSize: 18, fontFamily: "Tahoma" , textAlign: "center" }}>Schedule E-Mail</option>
//                         </Form.Control>
//                     </Form.Group>
//                     <div id="eb">
//                         <Button variant="primary" style={{ fontSize: 18, fontWeight: 'bold', fontFamily: "Tahoma" }} > Send </Button>
//                     </div>
//                 </div>
//             </div>

//         </div>)
// }

// export default Emailer;
