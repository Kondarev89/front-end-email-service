import React from "react";
import withRouter from "react-router-dom/withRouter";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown"
import { Form, FormControl, Button } from "react-bootstrap"
import "./Nav.css"

const Navigation = () => {
    return (
            <Navbar   id="nav">
                <Navbar.Brand href="/home" style={{ color: "white", fontSize: 22, fontWeight: 'bold', fontFamily: "Tahoma" }} id="n1"> E-Mail App </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Nav class="navbar-nav justify-content-between w-100" >
                <Nav.Link href="/home" style={{ color: "white", fontSize: 22, fontWeight: 'bold', fontFamily: "Tahoma" }} id="n2">Home</Nav.Link>
                    <Nav.Link href="/downtimesearch" style={{ color: "white", fontSize: 22, fontWeight: 'bold', fontFamily: "Tahoma" }} id="n2">Search For Clients</Nav.Link>
                    <Nav.Link href="/emailer" style={{ color: "white", fontSize: 22, fontWeight: 'bold', fontFamily: "Tahoma" }} id="n3">Send E-Mail</Nav.Link>
                    <Nav.Link href="/emailstorage" style={{ color: "white"  , fontSize: 22, fontWeight: 'bold', fontFamily: "Tahoma" }} id="n4">Mail Box</Nav.Link>
                    <Button variant="primary"  style={{ fontSize: 18 ,fontWeight: 'bold', fontFamily: "Tahoma"}}> Logout </Button>
                   
                </Nav>
            </Navbar>
        
    );
};

export default withRouter(Navigation);
