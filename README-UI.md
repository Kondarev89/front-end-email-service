# **Getting Started with Email App**

## Welcome to E-Mail Application Guide! In this guide we will walk through all of the components that are implemented in the application till 3.18.2021 and explain what they do, how do they work and how to easily learn to be effective with them. 
</br>
</br>


## **Arfer we log in...**
</br>
</br>

# **Available Functionalities**</br>
</br>

![Image1](https://i.postimg.cc/m2W2Jrv8/p2.png)  
 ## **1.  Search for Clients - search list for all ouf our clients.**

 ## **2.  Emailer  - E-Mail service with for all our needs to write E-Mails.**
 
 ## **3. Email Box  - Mail Box for our Send of Scheduled E-Mails.**

</br>
</br>



# **Lets walk throught each component and explain its purpose :**</br>  

 ## **1. Search for Clients** :  

In this component we have a **big list of all our clients** with their provided information and what **service / solutions** we offer them. Firs on the **top left corner** is a **button** that provides on the **"Search for Clients"** page only **emails and information** about the people, clients or buisnesses with a specific **Service** that We provide for them like **"Hosting"** or **"Database"**.

![Image1](https://i.postimg.cc/htynD4YF/p1.png)  

Before or after **sorting the list** with the specific service We can use the **"Search"** strap in the **upper middle** part of the screen to do a specific search for a customer which email we already know and want to find.  We can press directly on the **client's email** to be redirected into our second component to instantly write him an **E-mail**.



 ## **2. Send Emails** :  
In the **"Send Emails"** page we have a simple **E-Mail writing tool**. In the first strap **"To E-Mail Address :"** we can simply write the e-mail adress of the client we want to reach. Writing to **multiple recipients** is possible with just writing more emails and pressing enter. Next strap is the **E-Mail Tittle** where we write the subject of our e-mail. Next the **"New Message :"** we have the message box where we write the **text/message** we want to send to our clients. It is fully functional with everything we need for writing emails. **Upload files, emojies, fonts , images and even a garbage bin** button to throw this message in the trash!

![Image2](https://i.postimg.cc/7hfwNZqg/p2.png)  

After that we have the option to **"Send E-Mails"** OR **"Schedule E-Mails"** to be sended in the future to our clients. With a simple click we can send the required **E-Mail** to the destination we want it to go! The **"Send"** button is of course self-explanatory, press it and **send or schedule** the message you just wrote!




 ## **3.Mail Box**  :
**"The Mail Box"** component collects all the **E-Mails** we **send or schedule** to be sended in the future. At the **upper left corner** of the page is the button **"Send / Schedule"** to select and display on the **table below** specific replies to our customers. Either **"Send"** emails or **"Scheduled"** to be sended.


![Image2](https://i.postimg.cc/hvBWyXXm/p3.png)  

In the **upper center** part of the page there is a **"Search"** strip to search for specific customers either for **Send E-Mails** or **Scheduled E-Mails**. And last but not least the table below shows us our customer information **(e-mail, problem, send of scheduled e-mail)** and the date we wrote or scheduled the message to them.

</br>
  
**Author Tanyo Kondarev**
</br> 

**03.18.2021**